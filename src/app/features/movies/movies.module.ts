import { NgModule } from '@angular/core';
import { MoviesComponent } from './movies.component';
import { MoviesRoutingModule } from './movies-routing.module';
import { TableComponent } from 'src/app/shared/components/table/table.component';
import { SharedModule } from 'src/app/shared/shared.module';
import { CommonModule } from '@angular/common';
@NgModule({
  declarations: [
    MoviesComponent,
    TableComponent,
  ],
  imports: [
    MoviesRoutingModule,
    SharedModule,
    CommonModule
  ],
  providers: []
})
export class MoviesModule { }
