import { NgModule } from '@angular/core';
import { UsersComponent } from './users.component';
import { UsersRoutingModule } from './users-routing.module';
import { TableComponent } from 'src/app/shared/components/table/table.component';
import { SharedModule } from 'src/app/shared/shared.module';
import { CommonModule } from '@angular/common';
@NgModule({
  declarations: [
    UsersComponent,
    TableComponent,
  ],
  imports: [
    UsersRoutingModule,
    SharedModule,
    CommonModule
  ],
  providers: []
})
export class UsersModule { }
